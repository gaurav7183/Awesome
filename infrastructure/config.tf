locals {
    project_id = "cicd-207712"
    region = "us-central1"
    zone = "us-central1-f"
}


provider "google" {
    credentials = "${file(".\\credential\\account.json")}"
    project     = "${local.project_id}"
    region      = "${local.region}"
}