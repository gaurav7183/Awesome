using System;
using System.Data.SqlClient;

namespace DataFeed
{
    class Program
    {
        static void Main(string[] args)
        {
             var hostname = "35.228.89.95";
            var password = "HelloMYSQL";
            var connString = $"Data Source={hostname};Initial Catalog=master;User ID=sa;Password={password};";
            Console.WriteLine("connection string {0}", connString);
            CheckDataConnectivity(connString);
            while(true)
            {
            
            }
        }
        
        public static void CheckDataConnectivity(string connectionString)
        {
            Console.WriteLine("connection string {0}", connectionString);
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                Console.WriteLine("Connection open");
                try
                {
                    using (SqlCommand command = new SqlCommand("SELECT * FROM customer", con))
                    {
                        var data = command.ExecuteReader();
                        while (data.Read())
                        {
                            var firstname = data["FirstName"];
                            var lastname = data["LastName"];
                            var city = data["City"];
                            var country = data["Country"];
                            var phone = data["Phone"];
                            Console.WriteLine("FirstName : {0}, LastName : {1}, City : {2}, Country : {3}, Phone : {4}", firstname, lastname, city, country, phone);
                        }
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Something went wrong {0}", ex);
                }
            }
        }
    }
}
